FROM openjdk:13-jdk AS app

ENV GRADLE_OPTS -Dorg.gradle.deamon=false
COPY . /build
WORKDIR /build
RUN ./gradlew build