package cz.cvut.fit.dosoudom.sbackend2.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HomeController {

    @GetMapping
    fun home() = "Hello world"
}