package cz.cvut.fit.dosoudom.sbackend2.service

import cz.cvut.fit.dosoudom.sbackend2.dto.CreatePersonDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.PersonDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.UpdatePersonDTO
import cz.cvut.fit.dosoudom.sbackend2.entity.Person
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemNotFound
import cz.cvut.fit.dosoudom.sbackend2.exception.JPAIdMissing
import cz.cvut.fit.dosoudom.sbackend2.repository.PersonRepository
import cz.cvut.fit.dosoudom.sbackend2.repository.SkillRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class PersonService(
    @Autowired
    val personRepository: PersonRepository,

    @Autowired
    val skillRepository: SkillRepository,
) {
    private fun toDTO(person: Person)
        = PersonDTO(
            person.id ?: throw JPAIdMissing(),
            person.name,
            person.skills.map { it.id ?: throw JPAIdMissing() },
        )

    fun get(id: Long): PersonDTO? = personRepository.findByIdOrNull(id)?.let { toDTO(it) }

    fun list() = personRepository.findAll().map { toDTO(it) }

    fun list(skillIds: List<Long>)
        = if (skillIds.isNotEmpty()) {
            personRepository.findAllBySkillIds(skillIds, skillIds.size.toLong()).map { toDTO(it) }
        } else {
            list()
        }

    fun create(person: CreatePersonDTO): PersonDTO
        = toDTO(personRepository.save(Person(
            person.name,
            skillRepository.findAllById(person.skillIds),
        )))

    @Transactional
    fun update(id: Long, person: UpdatePersonDTO): PersonDTO {
        val p = personRepository.findByIdOrNull(id) ?: throw ItemNotFound()
        p.name = person.name
        return toDTO(p)
    }

    @Transactional
    fun delete(id: Long) {
        val p = personRepository.findByIdOrNull(id) ?: throw ItemNotFound()
        p.projects.forEach {
            it.persons = it.persons.filter { person -> person.id != p.id }
        }
        personRepository.delete(p)
    }

    fun hasSkill(personId: Long, skillId: Long): Boolean
        = (personRepository.findByIdOrNull(personId) ?: throw ItemNotFound())
            .skills.map { it.id ?: JPAIdMissing() }
            .contains(skillId)

    fun addSkill(personId: Long, skillId: Long): PersonDTO {
        val p = personRepository.findByIdOrNull(personId) ?: throw ItemNotFound()
        if (!hasSkill(personId, skillId)) {
            val s = skillRepository.findByIdOrNull(skillId) ?: throw ItemNotFound()
            p.skills += s
            personRepository.save(p)
        }
        return toDTO(p)
    }

    fun removeSkill(personId: Long, skillId: Long): PersonDTO {
        val p = personRepository.findByIdOrNull(personId) ?: throw ItemNotFound()
        p.skills = p.skills.filter { (it.id ?: throw JPAIdMissing()) != skillId }
        personRepository.save(p)
        return toDTO(p)
    }
}