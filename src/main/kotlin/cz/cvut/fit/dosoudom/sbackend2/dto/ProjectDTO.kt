package cz.cvut.fit.dosoudom.sbackend2.dto

data class ProjectDTO(
    val id: Long,
    val name: String,
    val manDayEstimate: Long?,
    val personIds: List<Long>,
)

data class CreateProjectDTO (
    val name: String,
    val manDayEstimate: Long?,
    val personIds: List<Long>,
)
