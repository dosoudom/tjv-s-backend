package cz.cvut.fit.dosoudom.sbackend2.service

import cz.cvut.fit.dosoudom.sbackend2.dto.CreateProjectDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.ProjectDTO
import cz.cvut.fit.dosoudom.sbackend2.entity.Project
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemAlreadyExists
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemNotFound
import cz.cvut.fit.dosoudom.sbackend2.exception.JPAIdMissing
import cz.cvut.fit.dosoudom.sbackend2.repository.PersonRepository
import cz.cvut.fit.dosoudom.sbackend2.repository.ProjectRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ProjectService(
    @Autowired val projectRepository: ProjectRepository,
    @Autowired val personRepository: PersonRepository,
) {
    private fun toDTO(project: Project): ProjectDTO = ProjectDTO(
        project.id ?: throw JPAIdMissing(),
        project.name,
        project.manDayEstimate,
        project.persons.map { it.id ?: throw JPAIdMissing() },
    )

    fun get(id: Long): ProjectDTO? = projectRepository.findByIdOrNull(id)?.let { toDTO(it) }

    fun list(): List<ProjectDTO> = projectRepository.findAll().map { toDTO(it) }

    @Transactional
    fun create(project: CreateProjectDTO): ProjectDTO
        = projectRepository.findByName(project.name)
            ?.let {  throw ItemAlreadyExists() }
            ?: toDTO(projectRepository.save(Project(
                project.name,
                project.manDayEstimate,
                personRepository.findAllById(project.personIds),
            )))

    @Transactional
    fun update(id: Long, project: CreateProjectDTO): ProjectDTO {
        projectRepository.findByName(project.name).let {
            if (it != null && it.id != id) {
                throw ItemAlreadyExists()
            }
        }
        projectRepository.findByIdOrNull(id)
            ?.let {
                it.name = project.name
                it.manDayEstimate = project.manDayEstimate
                it.persons = personRepository.findAllById(project.personIds)
                return toDTO(it)
            }
            ?: throw ItemNotFound()
    }

    @Transactional
    fun delete(id: Long) {
        val s = projectRepository.findByIdOrNull(id) ?: throw ItemNotFound()
        projectRepository.delete(s)
    }

}