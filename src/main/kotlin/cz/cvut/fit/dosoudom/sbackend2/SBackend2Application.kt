package cz.cvut.fit.dosoudom.sbackend2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SBackend2Application

fun main(args: Array<String>) {
    runApplication<SBackend2Application>(*args)
}
