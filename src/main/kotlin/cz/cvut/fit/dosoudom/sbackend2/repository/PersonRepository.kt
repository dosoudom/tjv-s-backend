package cz.cvut.fit.dosoudom.sbackend2.repository

import cz.cvut.fit.dosoudom.sbackend2.entity.Person
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface PersonRepository : JpaRepository<Person, Long> {
    // credits: http://www.sergiy.ca/how-to-write-many-to-many-search-queries-in-mysql-and-hibernate/
    @Query(
        "select p from Person p " +
        "join p.skills s " +
        "where s.id in (:skillIds) " +
        "group by p " +
        "having count(s) = :skillsN"
    )
    fun findAllBySkillIds(skillIds: List<Long>, skillsN: Long): List<Person>
}