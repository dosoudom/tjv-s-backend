package cz.cvut.fit.dosoudom.sbackend2.controller

import org.springframework.http.ResponseEntity

interface CRUDController<IdT, GetT, CreateT, UpdateT> {
    fun list(): Iterable<GetT>

    fun get(id: IdT): ResponseEntity<GetT>

    fun create(item: CreateT): ResponseEntity<GetT>

    fun update(id: IdT, patch: UpdateT): ResponseEntity<GetT>

    fun delete(id: IdT)
}