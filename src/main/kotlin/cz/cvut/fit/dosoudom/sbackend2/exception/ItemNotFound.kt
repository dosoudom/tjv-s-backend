package cz.cvut.fit.dosoudom.sbackend2.exception

import java.lang.RuntimeException

class ItemNotFound : RuntimeException()
