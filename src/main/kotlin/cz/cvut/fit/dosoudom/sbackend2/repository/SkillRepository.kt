package cz.cvut.fit.dosoudom.sbackend2.repository

import cz.cvut.fit.dosoudom.sbackend2.entity.Skill
import org.springframework.data.jpa.repository.JpaRepository

interface SkillRepository : JpaRepository<Skill, Long> {
    fun findByName(name: String): Skill?
}