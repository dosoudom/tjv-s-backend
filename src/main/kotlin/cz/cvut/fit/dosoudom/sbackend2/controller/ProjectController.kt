package cz.cvut.fit.dosoudom.sbackend2.controller

import cz.cvut.fit.dosoudom.sbackend2.dto.CreateProjectDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.ProjectDTO
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemAlreadyExists
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemNotFound
import cz.cvut.fit.dosoudom.sbackend2.service.ProjectService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.server.mvc.linkTo
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/projects")
class ProjectController(
    @Autowired val projectService: ProjectService,
) : CRUDController<Long, ProjectDTO, CreateProjectDTO, CreateProjectDTO> {
    @GetMapping
    override fun list(): Iterable<ProjectDTO> = projectService.list()

    @GetMapping("/{id}")
    override fun get(@PathVariable id: Long): ResponseEntity<ProjectDTO>
        = projectService.get(id)?.let { ResponseEntity.ok(it) }
        ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)

    @PostMapping
    override fun create(@RequestBody item: CreateProjectDTO): ResponseEntity<ProjectDTO>
        = try {
            projectService.create(item).let {
                ResponseEntity
                    .created(linkTo<ProjectController> { get(it.id) }.toUri())
                    .body(it)
            }
        } catch (_: ItemAlreadyExists) {
            throw ResponseStatusException(HttpStatus.CONFLICT)
        }

    @PutMapping("/{id}")
    override fun update(@PathVariable id: Long, @RequestBody patch: CreateProjectDTO): ResponseEntity<ProjectDTO>
        = try {
            projectService.update(id, patch).let {
                ResponseEntity
                    .created(linkTo<ProjectController> { get(id) }.toUri())
                    .body(it)
            }
        } catch (_: ItemNotFound) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        } catch (_: ItemAlreadyExists) {
            throw ResponseStatusException(HttpStatus.CONFLICT)
        }

    @DeleteMapping("/{id}")
    override fun delete(@PathVariable id: Long)
        = try {
            projectService.delete(id)
        } catch (_: ItemNotFound) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }
}