package cz.cvut.fit.dosoudom.sbackend2.service

import cz.cvut.fit.dosoudom.sbackend2.dto.CreateSkillDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.SkillDTO
import cz.cvut.fit.dosoudom.sbackend2.entity.Skill
import cz.cvut.fit.dosoudom.sbackend2.exception.JPAIdMissing
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemAlreadyExists
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemNotFound
import cz.cvut.fit.dosoudom.sbackend2.repository.SkillRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class SkillService(
    @Autowired
    val skillRepository: SkillRepository,
) {
    private fun toDTO(skill: Skill): SkillDTO = SkillDTO(
        skill.id ?: throw JPAIdMissing(),
        skill.name,
    )

    fun list() = skillRepository.findAll().map { toDTO(it) }

    fun list(ids: List<Long>) = skillRepository.findAllById(ids).map { toDTO(it) }

    fun get(id: Long) = skillRepository.findByIdOrNull(id)?.let { toDTO(it) }

    @Transactional
    fun create(skill: CreateSkillDTO)
        // unique check could be also handled using db driver exception,
        // however every driver raises another exception
        // and postgres doesn't even have specific exception for unique constraint violation
        = skillRepository.findByName(skill.name)
            ?.let { throw ItemAlreadyExists() }
            ?: toDTO(skillRepository.save(Skill(skill.name)))

    @Transactional
    fun update(id: Long, skill: CreateSkillDTO): SkillDTO {
        skillRepository.findByName(skill.name).let {
            if (it != null && it.id != id) {
                throw ItemAlreadyExists()
            }
        }
        skillRepository.findByIdOrNull(id)
            ?.let {
                it.name = skill.name
                return toDTO(it)
            }
            ?: throw ItemNotFound()
    }

    @Transactional
    fun delete(id: Long) {
        val s = skillRepository.findByIdOrNull(id) ?: throw ItemNotFound()
        s.persons.forEach {
            it.skills = it.skills.filter { skill -> skill.id != id }
        }
        skillRepository.delete(s)
    }
}