package cz.cvut.fit.dosoudom.sbackend2.dto

data class SkillDTO(
    val id: Long,
    val name: String,
)

data class CreateSkillDTO(
    val name: String,
)
