package cz.cvut.fit.dosoudom.sbackend2.dto


// putting multiple classes in one file is generally bad practice
// however I believe that it's a bit more organized since these DTOs
// are similar


data class PersonDTO (
    val id: Long,
    val name: String,
    val skillIds: List<Long>,
)

data class CreatePersonDTO (
    val name: String,
    val skillIds: List<Long>,
)

data class UpdatePersonDTO (
    val name: String,
)
