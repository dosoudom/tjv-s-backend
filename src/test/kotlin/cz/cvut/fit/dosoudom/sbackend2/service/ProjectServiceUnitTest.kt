package cz.cvut.fit.dosoudom.sbackend2.service

import cz.cvut.fit.dosoudom.sbackend2.dto.CreateProjectDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.ProjectDTO
import cz.cvut.fit.dosoudom.sbackend2.entity.Project
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemAlreadyExists
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemNotFound
import cz.cvut.fit.dosoudom.sbackend2.repository.ProjectRepository
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import java.util.*

@SpringBootTest
class ProjectServiceUnitTest {
    @Autowired
    lateinit var projectService: ProjectService

    @MockBean
    lateinit var projectRepository: ProjectRepository

    companion object {
        private val pr1 = Project("Mining", 40, listOf(), 1)
        private val pr2 = Project("Farming", 3, listOf(), 2)
        private val pr3 = Project("Fighting", null, listOf(), 3)
        private val pr4 = Project("Magic", null, listOf(), 4)
    }

    @Test
    fun `list projects`() {
        given(projectRepository.findAll()).willReturn(listOf(pr1, pr2, pr3, pr4))

        assertEquals(
            listOf(
                ProjectDTO(pr1.id!!, pr1.name, pr1.manDayEstimate, pr1.persons.map { it.id!! }),
                ProjectDTO(pr2.id!!, pr2.name, pr2.manDayEstimate, pr2.persons.map { it.id!! }),
                ProjectDTO(pr3.id!!, pr3.name, pr3.manDayEstimate, pr3.persons.map { it.id!! }),
                ProjectDTO(pr4.id!!, pr4.name, pr4.manDayEstimate, pr4.persons.map { it.id!! })
            ),
            projectService.list(),
        )
    }

    @Test
    fun `find project`() {
        given(projectRepository.findById(2)).willReturn(Optional.of(pr2))

        projectService.get(2).let {
            assertEquals(pr2.id!!, it?.id)
            assertEquals(pr2.name, it?.name)
            assertEquals(pr2.manDayEstimate, it?.manDayEstimate)
            assertEquals(pr2.persons.map { p -> p.id!! }, it?.personIds)
        }
    }

    @Test
    fun `find project - nonexisting`() {
        given(projectRepository.findById(404)).willReturn(Optional.empty())

        assertEquals(null, projectService.get(404))
    }

    @Test
    fun `create project`() {
        given(projectRepository.save(eq(Project(pr4.name, pr4.manDayEstimate, pr4.persons)))).willReturn(pr4)

        assertEquals(
            ProjectDTO(pr4.id!!, pr4.name, pr4.manDayEstimate, pr4.persons.map { it.id!! }),
            projectService.create(CreateProjectDTO(pr4.name, null, listOf())),
        )
    }

    @Test
    fun `update project`() {
        given(projectRepository.findById(pr2.id!!)).willReturn(Optional.of(pr2))

        assertEquals(
            ProjectDTO(pr2.id!!, "Blowing things up", 20, pr2.persons.map { it.id!! }),
            projectService.update(pr2.id!!, CreateProjectDTO("Blowing things up", 20, listOf())),
        )
    }

    @Test
    fun `update project that does not exist`() {
        given(projectRepository.findById(404)).willReturn(Optional.empty())

        assertThrows<ItemNotFound> {
            projectService.update(404, CreateProjectDTO("Blowing things up", null, listOf()))
        }
    }

    @Test
    fun `update project with duplicate name`() {
        given(projectRepository.findByName(pr2.name)).willReturn(pr2)
        given(projectRepository.findById(pr3.id!!)).willReturn(Optional.of(pr3))

        assertThrows<ItemAlreadyExists> {
            projectService.update(pr3.id!!, CreateProjectDTO(pr2.name, 42, listOf()))
        }
    }

    @Test
    fun `update project with duplicate name but same id`() {
        given(projectRepository.findByName(pr2.name)).willReturn(pr2)
        given(projectRepository.findById(pr2.id!!)).willReturn(Optional.of(pr2))

        assertEquals(
            ProjectDTO(pr2.id!!, pr2.name, null, pr2.persons.map { it.id!! }),
            projectService.update(pr2.id!!, CreateProjectDTO(pr2.name, null, listOf())),
        )
    }

    @Test
    fun `delete project that does not exist`() {
        given(projectRepository.findById(404)).willReturn(Optional.empty())

        assertThrows<ItemNotFound> {
            projectService.delete(404)
        }
    }
}