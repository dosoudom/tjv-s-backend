package cz.cvut.fit.dosoudom.sbackend2.service

import cz.cvut.fit.dosoudom.sbackend2.dto.CreatePersonDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.PersonDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.UpdatePersonDTO
import cz.cvut.fit.dosoudom.sbackend2.entity.Person
import cz.cvut.fit.dosoudom.sbackend2.entity.Skill
import cz.cvut.fit.dosoudom.sbackend2.exception.ItemNotFound
import cz.cvut.fit.dosoudom.sbackend2.repository.PersonRepository
import cz.cvut.fit.dosoudom.sbackend2.repository.SkillRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.BDDMockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import java.util.*

@SpringBootTest
class PersonServiceUnitTest {
    companion object {
        private val s1 = Skill("Mining", 1)
        private val s2 = Skill("Farming", 2)
        private val s3 = Skill("Fighting", 3)
        private val s4 = Skill("Magic", 4)
        private val p1 = Person("Durborlum", listOf(s1, s3), listOf(), 1)
        private val p2 = Person("Erivami", listOf(s2), listOf(), 2)
        private val p3 = Person("Skamman", listOf(s4), listOf(), 3)
        private val p4 = Person("Gokraem", listOf(s1), listOf(), 4)
    }

    @Autowired
    private lateinit var personService: PersonService

    @MockBean
    private lateinit var personRepository: PersonRepository

    @MockBean
    private lateinit var skillRepository: SkillRepository

    @Test
    fun `get person`() {
        given(personRepository.findById(1)).willReturn(Optional.of(p1))

        assertEquals(
            PersonDTO(1, p1.name, p1.skills.map { it.id!! }),
            personService.get(1),
        )
        then(personRepository).should().findById(1)
    }

    @Test
    fun `list persons`() {
        given(personRepository.findAll()).willReturn(listOf(p1, p2, p3, p4))

        assertEquals(
            listOf(
                PersonDTO(p1.id!!, p1.name, p1.skills.map { it.id!! }),
                PersonDTO(p2.id!!, p2.name, p2.skills.map { it.id!! }),
                PersonDTO(p3.id!!, p3.name, p3.skills.map { it.id!! }),
                PersonDTO(p4.id!!, p4.name, p4.skills.map { it.id!! }),
            ),
            personService.list()
        )
        then(personRepository).should().findAll()
    }

    @Test
    fun `list persons by skill ids`() {
        given(personRepository.findAll()).willReturn(listOf(p1, p2, p3, p4))

        listOf<Long>(1, 3).let {
            given(personRepository.findAllBySkillIds(it, it.size.toLong())).willReturn(listOf(p1))
        }
        listOf<Long>().let {
            given(personRepository.findAllBySkillIds(it, it.size.toLong())).willReturn(listOf())
        }
        listOf<Long>(1).let {
            given(personRepository.findAllBySkillIds(it, it.size.toLong())).willReturn(listOf(p1, p4))
        }

        assertEquals(
            listOf(
                PersonDTO(p1.id!!, p1.name, p1.skills.map { it.id!! }),
            ),
            personService.list(listOf(1, 3))
        )
        assertEquals(
            listOf(
                PersonDTO(p1.id!!, p1.name, p1.skills.map { it.id!! }),
                PersonDTO(p2.id!!, p2.name, p2.skills.map { it.id!! }),
                PersonDTO(p3.id!!, p3.name, p3.skills.map { it.id!! }),
                PersonDTO(p4.id!!, p4.name, p4.skills.map { it.id!! }),
            ),
            personService.list(listOf())
        )
        assertEquals(
            listOf(
                PersonDTO(p1.id!!, p1.name, p1.skills.map { it.id!! }),
                PersonDTO(p4.id!!, p4.name, p4.skills.map { it.id!! }),
            ),
            personService.list(listOf(1))
        )
    }

    @Test
    fun `get nonexisting person`() {
        given(personRepository.findById(1)).willReturn(Optional.empty())

        assertEquals(
            personService.get(1),
            null
        )
    }

    @Test
    fun `create person`() {
        val p2WithoutId = Person(p2.name, p2.skills)
        given(skillRepository.findAllById(eq(p2.skills.map { it.id }))).willReturn(p2.skills)
        given(personRepository.save(any(Person::class.java)))
            .will { it1 -> Person::class.javaObjectType.cast(it1.arguments[0]).let { Person(it.name, it.skills, listOf(), 2) } }

        assertEquals(
            PersonDTO(p2.id!!, p2.name, p2.skills.map { it.id!! }),
            personService.create(CreatePersonDTO(p2.name, p2.skills.map { it.id!! })),
        )
        then(skillRepository).should().findAllById(p2WithoutId.skills.map { it.id })
    }

    @Test
    fun `update person`() {
        given(personRepository.findById(3)).willReturn(Optional.of(p3))

        assertEquals(
            PersonDTO(3, "Thelgir", p3.skills.map { it.id!! }),
            personService.update(3, UpdatePersonDTO("Thelgir")),
        )
    }

    @Test
    fun `update person that does not exist`() {
        given(personRepository.findById(404)).willReturn(Optional.empty())

        assertThrows<ItemNotFound> {
            personService.update(404, UpdatePersonDTO("Gasdruirgit"))
        }
    }

    @Test
    fun `delete person that does not exist`() {
        given(personRepository.findById(404)).willReturn(Optional.empty())

        assertThrows<ItemNotFound> {
            personService.delete(404)
        }
    }

    @Test
    fun `check if person posses skill`() {
        given(personRepository.findById(1)).willReturn(Optional.of(p1))

        assertEquals(
            true,
            personService.hasSkill(1, 3)
        )

        assertEquals(
            false,
            personService.hasSkill(1, 4)
        )
    }
}