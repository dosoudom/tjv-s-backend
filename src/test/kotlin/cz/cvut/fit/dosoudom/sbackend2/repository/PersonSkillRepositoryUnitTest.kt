package cz.cvut.fit.dosoudom.sbackend2.repository

import org.springframework.test.annotation.DirtiesContext

import cz.cvut.fit.dosoudom.sbackend2.entity.Person
import cz.cvut.fit.dosoudom.sbackend2.entity.Skill
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class PersonServiceUnitTest(
    @Autowired private val personRepository: PersonRepository,
    @Autowired private val skillRepository: SkillRepository,
) {

    companion object {
        val s1 = Skill("Mining", 1)
        val s2 = Skill("Fighting", 2)
        val s3 = Skill("Farming", 3)
        val p1 = Person("Sikhid", listOf(s1, s2), listOf(), 4)
        val p2 = Person("Wevaek", listOf(s3), listOf(), 5)
        val p3 = Person("Brokrurum", listOf(s2, s3), listOf(), 6)
    }

    @Test
    fun `finds all persons having all skills`() {
        skillRepository.save(s1)
        skillRepository.save(s2)
        skillRepository.save(s3)
        personRepository.save(p1)
        personRepository.save(p2)
        personRepository.save(p3)

        assertEquals(listOf<Person>(), personRepository.findAllBySkillIds(listOf(1, 2, 3), 3))
        assertEquals(
            listOf(p2, p3).map { it.id },
            personRepository.findAllBySkillIds(listOf(3), 1).map { it.id }
        )
        assertEquals(
            listOf(p1).map { it.id },
            personRepository.findAllBySkillIds(listOf(1, 2), 2).map { it.id }
        )
    }
}