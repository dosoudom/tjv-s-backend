package cz.cvut.fit.dosoudom.sbackend2.controller

import cz.cvut.fit.dosoudom.sbackend2.dto.CreatePersonDTO
import cz.cvut.fit.dosoudom.sbackend2.dto.CreateProjectDTO
import cz.cvut.fit.dosoudom.sbackend2.service.PersonService
import cz.cvut.fit.dosoudom.sbackend2.service.ProjectService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.*
import org.springframework.test.annotation.DirtiesContext

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class ProjectControllerIntegrationTest {
    @Autowired lateinit var restTemplate: TestRestTemplate

    companion object {
        const val URL = "/projects"
    }

    @BeforeEach
    fun initEach(@Autowired projectService: ProjectService, @Autowired personService: PersonService) {
        personService.create(CreatePersonDTO("Dhomnorlum Shatterdelver", listOf()))
        personService.create(CreatePersonDTO("Weggaek Gravelcoat", listOf()))
        personService.create(CreatePersonDTO("Luthunli Drakegrog", listOf()))

        projectService.create(CreateProjectDTO("Forge", 120, listOf(1)))
        projectService.create(CreateProjectDTO("Diamond mine", 400, listOf(2, 3)))
        projectService.create(CreateProjectDTO("Army", null, listOf(1, 3)))
    }

    @Test
    fun `list projects`() {
        restTemplate.getForEntity(URL, String::class.java).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "[" +
                    "{\"id\":4,\"name\":\"Forge\",\"manDayEstimate\":120,\"personIds\":[1]}," +
                    "{\"id\":5,\"name\":\"Diamond mine\",\"manDayEstimate\":400,\"personIds\":[2,3]}," +
                    "{\"id\":6,\"name\":\"Army\",\"manDayEstimate\":null,\"personIds\":[1,3]}" +
                    "]",
                it.body,
            )
        }
    }

    @Test
    fun `get project`() {
        restTemplate.getForEntity("$URL/{id}", String::class.java, 4).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "{\"id\":4,\"name\":\"Forge\",\"manDayEstimate\":120,\"personIds\":[1]}",
                it.body,
            )
        }
    }

    @Test
    fun `get nonexisting project`() {
        assertEquals(
            HttpStatus.NOT_FOUND,
            restTemplate.getForEntity("$URL/{id}", String::class.java, 404).statusCode,
        )
    }

    @Test
    fun `create project`(@LocalServerPort serverPort: Int) {
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("{\"name\": \"Tavern\", \"manDayEstimate\": 50, \"personIds\": [1]}", h)
        restTemplate.postForEntity(URL, r, String::class.java).let {
            assertEquals(HttpStatus.CREATED, it.statusCode)
            Assertions.assertTrue(it.headers.containsKey("Location"))
            assertEquals("[http://localhost:$serverPort${URL}/7]", it.headers["Location"]!!.toString())
            assertEquals("{\"id\":7,\"name\":\"Tavern\",\"manDayEstimate\":50,\"personIds\":[1]}", it.body)
        }

        restTemplate.getForEntity("$URL/{id}", String::class.java, 7).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "{\"id\":7,\"name\":\"Tavern\",\"manDayEstimate\":50,\"personIds\":[1]}",
                it.body,
            )
        }
    }

    @Test
    fun `create project with nonexisting person`(@LocalServerPort serverPort: Int) {
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("{\"name\": \"Tavern\", \"manDayEstimate\": 50, \"personIds\": [404]}", h)
        restTemplate.postForEntity(URL, r, String::class.java).let {
            assertEquals(HttpStatus.CREATED, it.statusCode)
            Assertions.assertTrue(it.headers.containsKey("Location"))
            assertEquals("[http://localhost:$serverPort${URL}/7]", it.headers["Location"]!!.toString())
            assertEquals("{\"id\":7,\"name\":\"Tavern\",\"manDayEstimate\":50,\"personIds\":[]}", it.body)
        }

        restTemplate.getForEntity("$URL/{id}", String::class.java, 7).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "{\"id\":7,\"name\":\"Tavern\",\"manDayEstimate\":50,\"personIds\":[]}",
                it.body,
            )
        }
    }

    @Test
    fun `create project with duplicate name`() {
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("{\"name\": \"Forge\", \"manDayEstimate\": 50, \"personIds\": []}", h)
        assertEquals(
            HttpStatus.CONFLICT,
            restTemplate.postForEntity(URL, r, String::class.java).statusCode,
        )
    }

    @Test
    fun `delete project`() {
        assertEquals(
            HttpStatus.OK,
            restTemplate
                .exchange("$URL/{id}", HttpMethod.DELETE, HttpEntity.EMPTY, String::class.java, 4)
                .statusCode
        )
        restTemplate.getForEntity(URL, String::class.java).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "[" +
                    "{\"id\":5,\"name\":\"Diamond mine\",\"manDayEstimate\":400,\"personIds\":[2,3]}," +
                    "{\"id\":6,\"name\":\"Army\",\"manDayEstimate\":null,\"personIds\":[1,3]}" +
                    "]",
                it.body,
            )
        }
    }

    @Test
    fun `delete nonexisting project`() {
        assertEquals(
            HttpStatus.NOT_FOUND,
            restTemplate
                .exchange("$URL/{id}", HttpMethod.DELETE, HttpEntity.EMPTY, String::class.java, 404)
                .statusCode
        )
    }

    @Test
    fun `update project`(@LocalServerPort serverPort: Int) {
        val skillId = 4
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("{\"name\":\"Build forge\", \"manDayEstimate\": 123, \"personIds\":[1,2]}", h)
        restTemplate.exchange("$URL/{id}", HttpMethod.PUT, r, String::class.java, skillId).let {
            assertEquals(HttpStatus.CREATED, it.statusCode)
            Assertions.assertTrue(it.headers.containsKey("Location"))
            assertEquals("[http://localhost:$serverPort${URL}/$skillId]", it.headers["Location"]!!.toString())
            assertEquals(
                "{\"id\":4,\"name\":\"Build forge\",\"manDayEstimate\":123,\"personIds\":[1,2]}",
                it.body,
            )
        }

        restTemplate.getForEntity("$URL/{id}", String::class.java, 4).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "{\"id\":4,\"name\":\"Build forge\",\"manDayEstimate\":123,\"personIds\":[1,2]}",
                it.body,
            )
        }
    }

    @Test
    fun `update nonexisting project`() {
        val skillId = 404
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("{\"name\":\"Build forge\", \"manDayEstimate\": 123, \"personIds\":[1,2]}", h)
        assertEquals(
            HttpStatus.NOT_FOUND,
            restTemplate
                .exchange("$URL/{id}", HttpMethod.PUT, r, String::class.java, skillId)
                .statusCode,
        )
    }

    @Test
    fun `update project with duplicate name`() {
        val skillId = 4
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("{\"name\":\"Army\", \"manDayEstimate\": 123, \"personIds\":[1,2]}", h)
        assertEquals(
            HttpStatus.CONFLICT,
            restTemplate
                .exchange("$URL/{id}", HttpMethod.PUT, r, String::class.java, skillId)
                .statusCode,
        )
    }

    @Test
    fun `update project with nonexisting person`(@LocalServerPort serverPort: Int) {
        val skillId = 4
        val h = HttpHeaders()
        h.contentType = MediaType.APPLICATION_JSON
        val r = HttpEntity("{\"name\":\"Forge\", \"manDayEstimate\": 120, \"personIds\":[1,404]}", h)
        restTemplate.exchange("$URL/{id}", HttpMethod.PUT, r, String::class.java, skillId).let {
            assertEquals(HttpStatus.CREATED, it.statusCode)
            Assertions.assertTrue(it.headers.containsKey("Location"))
            assertEquals("[http://localhost:$serverPort${URL}/$skillId]", it.headers["Location"]!!.toString())
            assertEquals(
                "{\"id\":4,\"name\":\"Forge\",\"manDayEstimate\":120,\"personIds\":[1]}",
                it.body,
            )
        }

        restTemplate.getForEntity("$URL/{id}", String::class.java, 4).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "{\"id\":4,\"name\":\"Forge\",\"manDayEstimate\":120,\"personIds\":[1]}",
                it.body,
            )
        }
    }

    @Test
    fun `deleting person will remove it from projects`() {
        assertEquals(
            HttpStatus.OK,
            restTemplate
                .exchange(
                    "${PersonControllerIntegrationTest.URL}/{id}",
                    HttpMethod.DELETE,
                    HttpEntity.EMPTY,
                    String::class.java,
                    1,
                )
                .statusCode,
        )
        restTemplate.getForEntity(URL, String::class.java).let {
            assertEquals(HttpStatus.OK, it.statusCode)
            assertEquals(
                "[" +
                    "{\"id\":4,\"name\":\"Forge\",\"manDayEstimate\":120,\"personIds\":[]}," +
                    "{\"id\":5,\"name\":\"Diamond mine\",\"manDayEstimate\":400,\"personIds\":[2,3]}," +
                    "{\"id\":6,\"name\":\"Army\",\"manDayEstimate\":null,\"personIds\":[3]}" +
                    "]",
                it.body,
            )
        }
    }
}